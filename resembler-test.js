const compareImages = require('resemblejs/compareImages');
const fs = require("mz/fs");

async function getDiff(before, after, result){
    const options = {

         output: {
            errorColor: {
                red: 255,
                green: 0,
                blue: 255
            },
            errorType: "movement",
            transparency: 0.3,
            largeImageThreshold: 1200,
            useCrossOrigin: false,
            outputDiff: true
        },
        scaleToSameSize: true,
        ignore: ['less'],
    };

    const data = await compareImages(
        await fs.readFile(before),
        await fs.readFile(after),
        options
    );

    await fs.writeFile(result, data.getBuffer());
}

var before = './snapshot_account_1.png'
var after = './snapshot_account_2.png'
var result = './result_1.png'
getDiff(before, after, result);

var before = './snapshot_login_1.png'
var after = './snapshot_login_2.png'
var result = './result_2.png'
getDiff(before, after, result);

var before = './snapshot_search_1.png'
var after = './snapshot_search_2.png'
var result = './result_3.png'
getDiff(before, after, result);
